from test.integration.conftest import mock_callback
from test.unit.conftest import CardFactory

import pytest
from injector import Injector
from telegram import Update
from telegram.ext import CallbackContext

from app.data_access.models.bank_account import BankAccount
from app.domain.services.user_service import UserService
from app.infrastructure.message_constants.error_messages import NO_PERMISSIONS, USER_ALREADY_HAS_STARTED_CHATTING
from app.infrastructure.message_constants.response_messages import PHONE_SAVED, SUCCESS
from app.telegram_bot.handlers.base_commands_handlers import BaseHandlers
from app.telegram_bot.handlers.favorite_users_handlers import FavoriteUsersHandlers
from app.telegram_bot.handlers.money_transfer_handlers import MoneyTransferHandlers

injector = Injector()
user_service = injector.get(UserService)
base_commands_handlers = injector.get(BaseHandlers)
favorite_users_handlers = injector.get(FavoriteUsersHandlers)
money_transfer_handlers = injector.get(MoneyTransferHandlers)


@pytest.mark.django_db
@pytest.mark.integration
def test_can_not_use_me_command_without_phone(test_telegram_update: Update):
    base_commands_handlers.cmd_me(test_telegram_update, None)

    test_telegram_update.message.reply_text.assert_called_once_with(NO_PERMISSIONS)


@pytest.mark.django_db
@pytest.mark.integration
def test_start_should_create_user_if_not_exists(test_telegram_update: Update):
    base_commands_handlers.cmd_start(test_telegram_update, None)

    test_telegram_update.message.reply_text.assert_called_once_with(
        f"Hello {test_telegram_update.message.from_user.username}"
    )


@pytest.mark.django_db
@pytest.mark.integration
def test_should_not_create_user_twice(test_telegram_update: Update):
    base_commands_handlers.cmd_start(test_telegram_update, None)

    base_commands_handlers.cmd_start(test_telegram_update, None)

    test_telegram_update.message.reply_text.assert_called_with(USER_ALREADY_HAS_STARTED_CHATTING)


@pytest.mark.django_db
@pytest.mark.integration
def test_me_should_not_accept_user_without_phone(test_telegram_update: Update):
    base_commands_handlers.cmd_me(test_telegram_update, None)

    test_telegram_update.message.reply_text.assert_called_once_with(NO_PERMISSIONS)


@pytest.mark.django_db
@pytest.mark.integration
def test_save_phone_should_return_success(
    test_telegram_update: Update, test_callback_context_with_phone: CallbackContext
):
    base_commands_handlers.cmd_start(test_telegram_update, test_callback_context_with_phone)

    base_commands_handlers.cmd_set_phone(test_telegram_update, test_callback_context_with_phone)

    test_telegram_update.message.reply_text.assert_called_with(PHONE_SAVED)


@pytest.mark.django_db
@pytest.mark.integration
def test_transfer_money(test_telegram_update: Update, test_callback_context_with_phone: CallbackContext):
    base_commands_handlers.cmd_start(test_telegram_update, test_callback_context_with_phone)
    base_commands_handlers.cmd_set_phone(test_telegram_update, test_callback_context_with_phone)
    user = user_service.get_user(123)
    bank_account = BankAccount.objects.create(id=2, user=user.value)
    CardFactory.create_card(2, bank_account, 10)

    callback = mock_callback("2", "2", 2)
    money_transfer_handlers.cmd_transfer_user_money(test_telegram_update, callback)

    test_telegram_update.message.reply_text.assert_called_with(SUCCESS)


@pytest.mark.django_db
@pytest.mark.integration
def test_add_to_friends(test_telegram_update: Update, test_callback_context_with_phone: CallbackContext):
    base_commands_handlers.cmd_start(test_telegram_update, test_callback_context_with_phone)
    base_commands_handlers.cmd_set_phone(test_telegram_update, test_callback_context_with_phone)
    other_user = user_service.create_user(2)

    callback = mock_callback(other_user.value.id)
    favorite_users_handlers.cmd_add_to_favorites(test_telegram_update, callback)

    user = user_service.get_user(123)
    test_telegram_update.message.reply_text.assert_called_with(SUCCESS)
    assert user.value.favorites
