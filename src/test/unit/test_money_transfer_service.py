from test.unit.conftest import CardFactory

import pytest

from app.data_access.models.bank_account import BankAccount
from app.data_access.models.card import Card
from app.data_access.models.telegram_user import TelegramUser
from app.domain.services.money_transfer_service import MoneyTransferService
from app.domain.services.password_service import PasswordService
from app.domain.services.user_service import UserService
from app.infrastructure.message_constants.error_messages import (
    NOT_ENOUGH_MONEY_FOR_TRANSFER,
    USER_CARD_NOT_FOUND,
    USER_NOT_IN_FAVORITES,
)
from app.infrastructure.message_constants.response_messages import SUCCESS

password_service = PasswordService()
user_service = UserService(password_service)
sut = MoneyTransferService(user_service)


@pytest.mark.django_db
@pytest.mark.unit
def test_should_not_transfer_when_card_from_not_exists(test_user: TelegramUser):
    transfer_result = sut.transfer_money(test_user.id, 1, 2, 10)

    assert not transfer_result.success
    assert transfer_result.error_message == USER_CARD_NOT_FOUND


@pytest.mark.django_db
@pytest.mark.unit
def test_should_not_transfer_when_card_to_not_exists(test_card: Card):
    transfer_result = sut.transfer_money(test_card.bank_account.user.id, test_card.id, 1488, 10)

    assert not transfer_result.success
    assert transfer_result.error_message == USER_CARD_NOT_FOUND


@pytest.mark.django_db
@pytest.mark.unit
def test_should_not_transfer_when_not_enough_money(test_card: Card):
    transfer_result = sut.transfer_money(
        test_card.bank_account.user.id, test_card.id, test_card.id, test_card.money + 1
    )

    assert not transfer_result.success
    assert transfer_result.error_message == NOT_ENOUGH_MONEY_FOR_TRANSFER


@pytest.mark.django_db
@pytest.mark.unit
def test_should_transfer_to_himself(test_card: Card):
    balance = test_card.money
    amount = 5
    transfer_result = sut.transfer_money(test_card.bank_account.user.id, test_card.id, test_card.id, amount)
    card = user_service.get_card(test_card.id)

    assert transfer_result.success
    assert transfer_result.value == SUCCESS
    assert card.value.money == balance + amount


@pytest.mark.django_db
@pytest.mark.unit
def test_should_not_transfer_to_not_favorite_user(test_card: Card):
    user = TelegramUser.objects.create(id=2)
    bank_account = BankAccount.objects.create(id=2, user=user)
    card = CardFactory.create_card(2, bank_account, 10)

    transfer_result = sut.transfer_money(test_card.bank_account.user.id, test_card.id, card.id, 1)

    assert not transfer_result.success
    assert transfer_result.error_message == USER_NOT_IN_FAVORITES


@pytest.mark.django_db
@pytest.mark.unit
def test_should_transfer_to_favorite_user(test_card: Card):
    user = TelegramUser.objects.create(id=2)
    bank_account = BankAccount.objects.create(id=2, user=user)
    card = CardFactory.create_card(2, bank_account, 10)

    user.favorites.add(test_card.bank_account.user)
    user.save()
    transfer_result = sut.transfer_money(user.id, card.id, test_card.id, 1)

    assert transfer_result.success
    assert user_service.get_card(card.id).value.money == card.money - 1
    assert user_service.get_card(test_card.id).value.money == card.money + 1
