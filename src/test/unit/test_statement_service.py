from functools import reduce
from test.unit.conftest import CardFactory

import pytest

from app.data_access.models.bank_account import BankAccount
from app.data_access.models.card import Card
from app.data_access.models.telegram_user import TelegramUser
from app.domain.services.money_transfer_service import MoneyTransferService
from app.domain.services.password_service import PasswordService
from app.domain.services.statements_service import RECORDS_LIMIT, StatementsService
from app.domain.services.user_service import UserService

password_service = PasswordService()
user_service = UserService(password_service)
money_transfer_service = MoneyTransferService(user_service)

sut = StatementsService(user_service)


@pytest.mark.django_db
def test_should_return_card_statement(test_card: Card):
    amount = 1
    user = TelegramUser.objects.create(id=2)
    bank_account = BankAccount.objects.create(id=2, user=user)
    card = CardFactory.create_card(2, bank_account, 10)
    user.favorites.add(test_card.bank_account.user)
    user.save()
    money_transfer_service.transfer_money(user.id, card.id, test_card.id, amount)

    result = list(sut.get_card_statement(card.id))

    assert result is not None
    assert len(result) == 1
    assert result[0].from_card == card
    assert result[0].to_card == test_card
    assert result[0].amount == amount


@pytest.mark.django_db
def test_should_return_only_part_of_card_statements(test_card: Card):
    user = TelegramUser.objects.create(id=2)
    bank_account = BankAccount.objects.create(id=2, user=user)
    card = CardFactory.create_card(2, bank_account, RECORDS_LIMIT)
    user.favorites.add(test_card.bank_account.user)
    user.save()

    for i in range(RECORDS_LIMIT * 2):
        money_transfer_service.transfer_money(user.id, card.id, test_card.id, 1)
    result = list(sut.get_card_statement(card.id))

    assert len(result) == RECORDS_LIMIT
    assert reduce(lambda res, value: res + value.amount, result, 0) == RECORDS_LIMIT


@pytest.mark.django_db
def test_should_return_bank_account_statements(test_card: Card):
    money_transfer_service.transfer_money(test_card.bank_account.user.id, test_card.id, test_card.id, 1)

    result = list(sut.get_bank_account_statement(test_card.bank_account.id))

    assert result is not None
    assert len(result) == 1
    assert result[0].from_card == test_card


@pytest.mark.django_db
def test_should_return_only_part_of_bank_account_statements(test_card: Card):
    for i in range(RECORDS_LIMIT * 2):
        money_transfer_service.transfer_money(test_card.bank_account.user.id, test_card.id, test_card.id, 1)

    result = list(sut.get_bank_account_statement(test_card.bank_account.id))

    assert len(result) == RECORDS_LIMIT
    assert reduce(lambda res, value: res + value.amount, result, 0) == RECORDS_LIMIT


@pytest.mark.django_db
def test_should_return_statements_usernames(test_card: Card):
    user = TelegramUser.objects.create(id=2, username="test")
    bank_account = BankAccount.objects.create(id=2, user=user)
    card = CardFactory.create_card(2, bank_account, 10)

    test_card.bank_account.user.favorites.add(user)
    test_card.bank_account.user.save()
    money_transfer_service.transfer_money(test_card.bank_account.user.id, test_card.id, card.id, 1)

    result = list(sut.get_statement_users_names(test_card.bank_account.user.id))

    assert len(result) == 1
    assert result[0] == "test"
