import pytest

from app.data_access.models.telegram_user import TelegramUser
from app.domain.services.favorite_users_service import FavoriteUsersService
from app.domain.services.password_service import PasswordService
from app.domain.services.user_service import UserService
from app.infrastructure.message_constants.error_messages import NO_FAVORITES, USER_ALREADY_IN_FAVORITES, USER_NOT_FOUND

password_service = PasswordService()
user_service = UserService(password_service)
sut = FavoriteUsersService(user_service)


@pytest.mark.django_db
@pytest.mark.unit
def test_should_add_user_to_favorites(test_user: TelegramUser):
    other_user = user_service.create_user(2)

    result = sut.add_user_to_favorites(other_user.value.id, test_user.id)

    assert result.success


@pytest.mark.django_db
@pytest.mark.unit
def test_should_not_add_user_to_favorites_twice(test_user: TelegramUser):
    other_user = user_service.create_user(2)

    result = sut.add_user_to_favorites(other_user.value.id, test_user.id)
    result2 = sut.add_user_to_favorites(other_user.value.id, test_user.id)

    assert result.success
    assert not result2.success
    assert result2.error_message == USER_ALREADY_IN_FAVORITES


@pytest.mark.django_db
@pytest.mark.unit
def test_should_return_all_favorites(test_user: TelegramUser):
    user = user_service.create_user(2)
    other_user = user_service.create_user(3)

    result = sut.add_user_to_favorites(user.value.id, test_user.id)
    result2 = sut.add_user_to_favorites(user.value.id, other_user.value.id)

    favorites = sut.get_favorites(user.value.id)

    assert result.success
    assert result2.success
    assert favorites.success
    assert len(favorites.value) == 2


@pytest.mark.django_db
@pytest.mark.unit
def test_should_fail_when_no_favorite_users(test_user: TelegramUser):
    favorites = sut.get_favorites(test_user.id)

    assert not favorites.success
    assert favorites.error_message == NO_FAVORITES


@pytest.mark.django_db
@pytest.mark.unit
def test_should_delete_favorite_user(test_user: TelegramUser):
    user = user_service.create_user(2)
    sut.add_user_to_favorites(user.value.id, test_user.id)

    delete_result = sut.delete_favorite_user(user.value.id, test_user.id)

    assert delete_result.success
    assert not len(user.value.favorites.all())


@pytest.mark.django_db
@pytest.mark.unit
def test_should_return_not_found_when_user_not_exists(test_user: TelegramUser):
    delete_result = sut.delete_favorite_user(test_user.id, 1488)

    assert not delete_result.success
    assert delete_result.error_message == USER_NOT_FOUND


@pytest.mark.django_db
@pytest.mark.unit
def test_should_return_success_when_user_not_in_favorites(test_user: TelegramUser):
    user = user_service.create_user(2)

    delete_result = sut.delete_favorite_user(user.value.id, test_user.id)

    assert delete_result.success
    assert not len(user.value.favorites.all())
