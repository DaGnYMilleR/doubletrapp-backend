from django.db import models

from app.data_access.models.telegram_user import TelegramUser


class BankAccount(models.Model):
    id = models.BigAutoField(auto_created=True, primary_key=True, serialize=False)
    user = models.ForeignKey(TelegramUser, on_delete=models.CASCADE)
