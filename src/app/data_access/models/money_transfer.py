from django.db import models

from app.data_access.models.card import Card


class MoneyTransfer(models.Model):
    id = models.BigAutoField(auto_created=True, primary_key=True, serialize=False)
    from_card = models.ForeignKey(Card, on_delete=models.CASCADE, related_name="from_card")
    to_card = models.ForeignKey(Card, on_delete=models.CASCADE, related_name="to_card")
    amount = models.DecimalField(max_digits=5, decimal_places=2)
    date_time = models.DateTimeField(auto_now=True)
