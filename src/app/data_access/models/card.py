from django.db import models

from app.data_access.models.bank_account import BankAccount


class Card(models.Model):
    id = models.BigAutoField(auto_created=True, primary_key=True, serialize=False)
    bank_account = models.ForeignKey(BankAccount, on_delete=models.CASCADE)
    money = models.DecimalField(max_digits=5, decimal_places=2)
