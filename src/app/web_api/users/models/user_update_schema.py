from ninja import Schema


class UserUpdateSchema(Schema):
    first_name: str
    last_name: str
    phone_number: str
