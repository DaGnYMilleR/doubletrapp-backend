from ninja.orm import create_schema

from app.data_access.models.telegram_user import TelegramUser

TelegramUserSchema = create_schema(TelegramUser, depth=1, exclude=["password_hash"])
