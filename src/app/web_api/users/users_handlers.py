from injector import inject
from ninja.errors import HttpError

from app.domain.dtos.update_user_dto import UpdateUserDto
from app.domain.services.favorite_users_service import FavoriteUsersService
from app.domain.services.phone_number_service import PhoneNumberService
from app.domain.services.user_service import UserService
from app.web_api.users.models.user_update_schema import UserUpdateSchema


class UsersHandlers:
    @inject
    def __init__(
        self,
        user_service: UserService,
        phone_number_service: PhoneNumberService,
        favorite_users_service: FavoriteUsersService,
    ):
        self.favorite_users_service = favorite_users_service
        self.phone_number_service = phone_number_service
        self.user_service = user_service

    def get_user_data(self, request, user_id: int):
        user_result = self.user_service.get_user(user_id)

        if not user_result.success:
            raise HttpError(404, "User not found")

        return user_result.value

    def set_phone(self, request, user_id: int, phone_number: str):
        result = self.phone_number_service.set_user_phone(user_id, phone_number)

        if not result.success:
            raise HttpError(400, result.error_message)

        return {"id": user_id}

    def edit_user(self, request, user_id: int, model: UserUpdateSchema):
        update_model = UpdateUserDto(model.first_name, model.last_name, model.phone_number)
        result = self.user_service.update_user(user_id, update_model)

        if not result.success:
            raise HttpError(400, result.error_message)

        return {"success": True}

    def add_favorite(self, request, user_id: int, other_user_id: int):
        result = self.favorite_users_service.add_user_to_favorites(user_id, other_user_id)

        if not result.success:
            raise HttpError(400, result.error_message)

        return {"success": True}

    def delete_favorite(self, request, user_id: int, other_user_id: int):
        result = self.favorite_users_service.delete_favorite_user(user_id, other_user_id)

        if not result.success:
            raise HttpError(400, result.error_message)

        return {"success": True}
