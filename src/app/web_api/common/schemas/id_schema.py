from ninja import Schema


class IdSchema(Schema):
    id: int
