from ninja import Schema


class StatusSchema(Schema):
    success: bool
