from injector import inject
from ninja.errors import HttpError

from app.web_api.auth.schemas.login import LoginModel
from app.web_api.auth.schemas.tokens import RefreshTokenModel
from app.web_api.auth.services.login_service import LoginUserService
from app.web_api.auth.services.tokens_service import TokensService


class AuthHandlers:
    @inject
    def __init__(self, login_service: LoginUserService, tokens_service: TokensService):
        self.tokens_service = tokens_service
        self.login_service = login_service

    def refresh(self, request, model: RefreshTokenModel):
        refresh_result = self.tokens_service.refresh_tokens(model)

        if not refresh_result.success:
            raise HttpError(403, refresh_result.error_message)

        return refresh_result.value

    def login(self, request, model: LoginModel):
        tokens_result = self.login_service.login_user(model)

        if not tokens_result.success:
            raise HttpError(403, "Forbidden")

        return tokens_result.value
