class AccessToken:
    def __init__(self, token: str, expires_at: float):
        self.token = token
        self.expires_at = expires_at


class RefreshToken:
    def __init__(self, token: str, expires_at: float):
        self.token = token
        self.expires_at = expires_at


class TokensPair:
    def __init__(self, access_token: AccessToken, refresh_token: RefreshToken):
        self.refresh_token = refresh_token
        self.access_token = access_token
