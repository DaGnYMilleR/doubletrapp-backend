from injector import inject
from ninja import Router

from app.web_api.auth.auth_handlers import AuthHandlers
from app.web_api.auth.schemas.tokens import TokensPairSchema


class AuthRouterFactory:
    @inject
    def __init__(self, handlers: AuthHandlers):
        self.handlers = handlers

    def create(self) -> Router:
        router = Router()

        router.add_api_operation("login", ["POST"], self.handlers.login, response=TokensPairSchema)

        router.add_api_operation("refresh", ["POST"], self.handlers.refresh, response=TokensPairSchema)
        return router
