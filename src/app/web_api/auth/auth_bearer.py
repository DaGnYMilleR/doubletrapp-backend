from datetime import datetime

from injector import inject
from ninja.errors import HttpError
from ninja.security import HttpBearer

from app.web_api.auth.services.tokens_service import EXPIRATION_CLAIM, TokensService


class AuthBearer(HttpBearer):
    @inject
    def __init__(self, tokens_service: TokensService):
        super().__init__()
        self.tokens_service = tokens_service

    def authenticate(self, request, token):
        payload = self.tokens_service.try_decode_token(token)
        if not payload.success:
            raise HttpError(401, "Unauthorized")
        if payload.value[EXPIRATION_CLAIM] < datetime.utcnow().timestamp():
            raise HttpError(401, "Unauthorized")

        return token
