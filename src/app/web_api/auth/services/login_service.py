from django.db.models import Q
from injector import inject

from app.data_access.models.issued_token import IssuedToken
from app.domain.services.password_service import PasswordService
from app.domain.services.user_service import UserService
from app.infrastructure.result import Fail, Result, Success
from app.web_api.auth.models.tokens import TokensPair
from app.web_api.auth.schemas.login import LoginModel
from app.web_api.auth.services.tokens_service import TokensService


class LoginUserService:
    @inject
    def __init__(self, user_service: UserService, tokens_service: TokensService, password_service: PasswordService):
        self.password_service = password_service
        self.tokens_service = tokens_service
        self.user_service = user_service

    def login_user(self, model: LoginModel) -> Result[TokensPair]:
        user_result = self.user_service.get_user(model.login)
        if not user_result.success:
            return Fail()

        if not self.password_service.check_password(user_result.value, model.password):
            return Fail()

        issued_token = IssuedToken.objects.filter(Q(user=user_result.value) & Q(revoked=False)).first()
        if issued_token:
            issued_token.revoked = True
            issued_token.save()

        return Success(self.tokens_service.generate_tokens(user_result.value))
