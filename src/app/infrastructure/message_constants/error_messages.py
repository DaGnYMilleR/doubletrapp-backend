from app.infrastructure.message_constants.command_names import SET_PHONE, START

USER_ALREADY_HAS_STARTED_CHATTING = "You have already started chatting with the bot :)"
INCORRECT_ARGUMENTS_NUMBER = "Incorrect number of arguments."
ENTER_ONLY_PHONE = "You need enter only your phone number."

USER_NOT_FOUND = "User not found"
INVALID_PHONE_NUMBER_FORMAT = "Invalid phone number format"

NOT_FOUND = "Not found"

USER_BANK_ACCOUNT_NOT_FOUND = "Bank account not found"
USER_CARD_NOT_FOUND = "Card not found"
NOT_ENOUGH_MONEY_FOR_TRANSFER = "Not enough money for transfer"
BANK_ACCOUNT_DOES_NOT_BELONGS_TO_USER = "Bank account doesn't belongs to user"
CARD_DOES_NOT_BELONGS_TO_USER = "Card doesn't belongs to user"

NOT_A_NUMBER = "Error while parsing '{0}' argument. Please enter number"

NO_FAVORITES = "No favorites"
USER_NOT_IN_FAVORITES = "User not in favorites"
USER_ALREADY_IN_FAVORITES = "User already in favorites"

NO_PERMISSIONS = (
    f"You have not permissions to use this command. Please use "
    f"/{START} and /{SET_PHONE} commands to fill info about yourself"
)
