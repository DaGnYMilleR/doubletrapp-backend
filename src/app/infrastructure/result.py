from typing import Generic, TypeVar

T = TypeVar("T")


class Result(Generic[T]):
    def __init__(self, success: bool, value: T, error_message: str = "") -> None:
        self.success = success
        self.value = value
        self.error_message = error_message


class Success(Result):
    def __init__(self, value: T = None):
        super().__init__(True, value)


class Fail(Result):
    def __init__(self, error_message: str = ""):
        super().__init__(False, None, error_message)
