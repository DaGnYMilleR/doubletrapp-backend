from django.db.models import Q
from injector import inject

from app.data_access.models.card import Card
from app.data_access.models.money_transfer import MoneyTransfer
from app.domain.services.user_service import UserService

RECORDS_LIMIT = 100


class StatementsService:
    @inject
    def __init__(self, user_service: UserService):
        self.user_service = user_service

    def get_card_statement(self, card_id: int) -> [MoneyTransfer]:
        card = self.user_service.get_card(card_id)
        if not card.success:
            return

        return MoneyTransfer.objects.filter(Q(from_card=card.value) | Q(to_card=card.value)).order_by("date_time")[
            :RECORDS_LIMIT
        ]

    def get_bank_account_statement(self, bank_account_id: int) -> [MoneyTransfer]:
        cards = list(Card.objects.filter(bank_account__id=bank_account_id))

        return MoneyTransfer.objects.filter(Q(from_card__in=cards) | Q(to_card__in=cards)).order_by("date_time")[
            :RECORDS_LIMIT
        ]

    def get_statement_users_names(self, user_id: int) -> [str]:
        recipients = MoneyTransfer.objects.filter(from_card__bank_account__user__id=user_id).values_list(
            "to_card__bank_account__user__username", flat=True
        )
        senders = MoneyTransfer.objects.filter(to_card__bank_account__user__id=user_id).values_list(
            "from_card__bank_account__user__username", flat=True
        )
        return recipients.union(senders)
