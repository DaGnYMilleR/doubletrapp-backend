import decimal

from django.db import transaction
from injector import inject

from app.data_access.models.card import Card
from app.data_access.models.money_transfer import MoneyTransfer
from app.data_access.models.telegram_user import TelegramUser
from app.domain.services.user_service import UserService
from app.infrastructure.message_constants.error_messages import NOT_ENOUGH_MONEY_FOR_TRANSFER, USER_NOT_IN_FAVORITES
from app.infrastructure.message_constants.response_messages import SUCCESS
from app.infrastructure.result import Fail, Result, Success


class MoneyTransferService:
    @inject
    def __init__(self, user_service: UserService):
        self.user_service = user_service

    def transfer_money(self, user_id: int, card_from: int, card_to: int, amount: float) -> Result[str]:
        user_result = self.user_service.get_user(user_id)
        if not user_result.success:
            return Fail(user_result.error_message)

        from_card_result = self.user_service.get_card(card_from)
        if not from_card_result.success:
            return Fail(from_card_result.error_message)

        to_card_result = self.user_service.get_card(card_to)
        if not to_card_result.success:
            return Fail(to_card_result.error_message)

        if from_card_result.value.money < amount:
            return Fail(NOT_ENOUGH_MONEY_FOR_TRANSFER)

        if not self.is_user_in_favorites(user_result.value, to_card_result.value) and card_to != card_from:
            return Fail(USER_NOT_IN_FAVORITES)

        decimal_amount = decimal.Decimal(amount)

        self.execute_transaction(from_card_result.value, to_card_result.value, decimal_amount)

        return Success(SUCCESS)

    @transaction.atomic
    def execute_transaction(self, from_card: Card, to_card: Card, decimal_amount: decimal) -> None:
        from_card.money -= decimal_amount
        to_card.money += decimal_amount
        transfer_record = MoneyTransfer(from_card=from_card, to_card=to_card, amount=decimal_amount)

        transfer_record.save()
        from_card.save()
        to_card.save()

    def is_user_in_favorites(self, user: TelegramUser, card: Card) -> bool:
        return user.favorites.filter(pk=card.bank_account.user.id).exists()
