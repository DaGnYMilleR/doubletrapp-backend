from annoying.functions import get_object_or_None
from django.contrib.auth.hashers import make_password
from django.db.models import Sum
from injector import inject

from app.data_access.models.bank_account import BankAccount
from app.data_access.models.card import Card
from app.data_access.models.telegram_user import TelegramUser
from app.domain.dtos.update_user_dto import UpdateUserDto
from app.domain.services.password_service import PasswordService
from app.domain.validators.phone_number_validator import PhoneNumberValidator
from app.infrastructure.message_constants.error_messages import (
    INVALID_PHONE_NUMBER_FORMAT,
    USER_BANK_ACCOUNT_NOT_FOUND,
    USER_CARD_NOT_FOUND,
    USER_NOT_FOUND,
)
from app.infrastructure.result import Fail, Result, Success


class UserService:
    @inject
    def __init__(self, password_service: PasswordService):
        self.password_service = password_service

    def create_user(
        self, user_id: int, username: str = None, first_name: str = None, last_name: str = None
    ) -> Result[TelegramUser]:
        user, user_exists = TelegramUser.objects.get_or_create(
            id=user_id,
            defaults={
                "username": username,
                "first_name": first_name,
                "last_name": last_name,
            },
        )

        return Success(user) if user_exists else Fail()

    def get_user(self, user_id: int) -> Result[TelegramUser]:
        user = get_object_or_None(TelegramUser, id=user_id)
        return Success(user) if user is not None else Fail(USER_NOT_FOUND)

    def update_user(self, user_id: int, model: UpdateUserDto) -> Result[None]:
        user_result = self.get_user(user_id)
        if not user_result.success:
            return Fail(user_result.error_message)

        if not PhoneNumberValidator.is_valid(model.phone_number):
            return Fail(INVALID_PHONE_NUMBER_FORMAT)

        user = user_result.value
        user.first_name = model.first_name
        user.last_name = model.last_name
        user.phone_number = model.phone_number
        user.save()

        return Success()

    def set_password(self, user_id: int, password: str):
        user_result = self.get_user(user_id)
        if not user_result.success:
            return Fail(user_result.error_message)

        user = user_result.value
        user.password_hash = self.password_service.make_password(user_id, password)
        user.save()

    def get_bank_account_balance(self, user_id: int) -> Result[float]:
        if not self.is_user_exists(user_id):
            return Fail(USER_NOT_FOUND)

        result = Card.objects.filter(bank_account__user=user_id).aggregate(total_money=Sum("money"))

        result_money = result["total_money"]

        return Success(result_money) if result_money is not None else Fail(USER_BANK_ACCOUNT_NOT_FOUND)

    def get_card_balance(self, user_id: int, card_id: int) -> Result[float]:
        if not self.is_user_exists(user_id):
            return Fail(USER_NOT_FOUND)

        card = Card.objects.filter(pk=card_id).values_list("money").first()

        return Success(card[0]) if card is not None else Fail(USER_CARD_NOT_FOUND)

    def get_card(self, card_id: int) -> Result[Card]:
        card = get_object_or_None(Card, id=card_id)

        return Success(card) if card is not None else Fail(USER_CARD_NOT_FOUND)

    def is_user_exists(self, user_id: int):
        return TelegramUser.objects.filter(pk=user_id).exists()

    def is_card_belongs_to_user(self, user_id: int, card_id: int) -> bool:
        return user_id in Card.objects.filter(pk=card_id).values_list("bank_account__user__id").first()

    def is_bank_account_belongs_to_user(self, user_id: int, bank_account_id: int) -> bool:
        return user_id in BankAccount.objects.filter(pk=bank_account_id).values_list("user__id").first()
