from injector import inject

from app.data_access.models.bank_account import BankAccount
from app.domain.services.user_service import UserService
from app.infrastructure.result import Fail, Result, Success


class BankAccountService:
    @inject
    def __init__(self, user_service: UserService):
        self.user_service = user_service

    def create_bank_account(self, user_id: int) -> Result[BankAccount]:
        user = self.user_service.get_user(user_id)
        if not user.success:
            return Fail(user.error_message)

        bank_account = BankAccount.objects.create(user=user.value)
        return Success(bank_account)
