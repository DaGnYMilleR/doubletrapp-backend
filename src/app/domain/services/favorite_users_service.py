from injector import inject

from app.data_access.models.telegram_user import TelegramUser
from app.domain.services.user_service import UserService
from app.infrastructure.message_constants.error_messages import NO_FAVORITES, USER_ALREADY_IN_FAVORITES
from app.infrastructure.message_constants.response_messages import SUCCESS
from app.infrastructure.result import Fail, Result, Success


class FavoriteUsersService:
    @inject
    def __init__(self, user_service: UserService):
        self.user_service = user_service

    def add_user_to_favorites(self, user_id: int, another_user_id: int) -> Result[str]:
        user_result = self.user_service.get_user(user_id)
        if not user_result.success:
            return Fail(user_result.error_message)

        another_user_result = self.user_service.get_user(another_user_id)
        if not another_user_result.success:
            return Fail(another_user_result.error_message)

        if another_user_result.value in user_result.value.favorites.all():
            return Fail(USER_ALREADY_IN_FAVORITES)

        user_result.value.favorites.add(another_user_result.value)
        user_result.value.save()

        return Success(SUCCESS)

    def get_favorites(self, user_id: int) -> Result[list[TelegramUser]]:
        user_result = self.user_service.get_user(user_id)
        if not user_result.success:
            return Fail(user_result.error_message)

        favorites = list(user_result.value.favorites.all())

        return Success(favorites) if favorites else Fail(NO_FAVORITES)

    def delete_favorite_user(self, user_id: int, another_user_id: int) -> Result[str]:
        user_result = self.user_service.get_user(user_id)
        if not user_result.success:
            return Fail(user_result.error_message)

        another_user_result = self.user_service.get_user(another_user_id)
        if not another_user_result.success:
            return Fail(another_user_result.error_message)

        user_result.value.favorites.remove(another_user_result.value)
        user_result.value.save()
        return Success(SUCCESS)
