from django.contrib import admin

from app.web_api.admin.admin_user import AdminUserAdmin
from app.web_api.admin.bank_account import BankAccountAdmin
from app.web_api.admin.card import CardAdmin
from app.web_api.admin.money_transfer import MoneyTransfer
from app.web_api.admin.telegram_user import TelegramUserAdmin

admin.site.site_title = "Backend course"
admin.site.site_header = "Backend course"
