from django.core.management.base import BaseCommand
from injector import Injector

from app.telegram_bot.bot import Bot, BotCommandsCreator
from config.settings import TELEGRAM_BOT_TOKEN


class Command(BaseCommand):
    help = "runs telegram bot"

    def handle(self, *args, **options):
        injector = Injector()
        bot = Bot(TELEGRAM_BOT_TOKEN)
        bot.configure(injector.get(BotCommandsCreator).create_commands())
        bot.run()
