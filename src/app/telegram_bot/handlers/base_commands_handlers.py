from injector import inject
from telegram import Update
from telegram.ext import CallbackContext

from app.domain.services.phone_number_service import PhoneNumberService
from app.domain.services.user_service import UserService
from app.infrastructure.message_constants.error_messages import USER_ALREADY_HAS_STARTED_CHATTING
from app.telegram_bot.decorators.phone_required import phone_required
from app.telegram_bot.decorators.require_args import require_args


class BaseHandlers:
    @inject
    def __init__(self, user_service: UserService, phone_number_service: PhoneNumberService):
        self.phone_number_service = phone_number_service
        self.user_service = user_service

    def cmd_start(self, update: Update, context: CallbackContext) -> None:
        user_id = update.message.from_user.id
        username = update.message.from_user.username
        first_name = update.message.from_user.first_name
        last_name = update.message.from_user.last_name

        created_user = self.user_service.create_user(user_id, username, first_name, last_name)

        if not created_user.success:
            update.message.reply_text(USER_ALREADY_HAS_STARTED_CHATTING)
            return

        update.message.reply_text(f"Hello {created_user.value.username}")

    @require_args(1)
    def cmd_set_phone(self, update: Update, context: CallbackContext) -> None:
        user_id = update.message.from_user.id

        phone_number = context.args[0]

        result = self.phone_number_service.set_user_phone(user_id, phone_number)

        if result.success:
            update.message.reply_text(result.value)
            return

        update.message.reply_text(result.error_message)

    @phone_required
    def cmd_me(self, update: Update, context: CallbackContext) -> None:
        user = self.user_service.get_user(update.message.from_user.id)

        update.message.reply_text(str(user.value))

    @phone_required
    @require_args(1)
    def cmd_set_password(self, update: Update, context: CallbackContext) -> None:
        user_id = update.message.from_user.id

        self.user_service.set_password(user_id, context.args[0])
