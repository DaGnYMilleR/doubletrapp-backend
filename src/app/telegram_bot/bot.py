import time
from typing import List

from injector import inject
from telegram.ext import CommandHandler, Updater

from app.infrastructure.message_constants.command_names import (
    ADD_TO_FAVORITES,
    GET_BANK_ACCOUNT_STATEMENT,
    GET_CARD_STATEMENT,
    GET_FAVORITES,
    GET_STATEMENTS_USERNAMES,
    ME,
    MONEY,
    REMOVE_FAVORITE,
    SET_PASSWORD,
    SET_PHONE,
    START,
    TRANSFER,
)
from app.telegram_bot.handlers.base_commands_handlers import BaseHandlers
from app.telegram_bot.handlers.favorite_users_handlers import FavoriteUsersHandlers
from app.telegram_bot.handlers.money_transfer_handlers import MoneyTransferHandlers
from config.settings import CERT_PATH, PRIV_KEY_PATH, WEBHOOK_URL


class BotCommandsCreator:
    @inject
    def __init__(
        self,
        base_handlers: BaseHandlers,
        favorite_users_handlers: FavoriteUsersHandlers,
        money_transfer_handlers: MoneyTransferHandlers,
    ):
        self.favorite_users_handlers = favorite_users_handlers
        self.money_transfer_handlers = money_transfer_handlers
        self.base_handlers = base_handlers

    def create_commands(self) -> List[CommandHandler]:
        yield CommandHandler(START, self.base_handlers.cmd_start)
        yield CommandHandler(SET_PHONE, self.base_handlers.cmd_set_phone)
        yield CommandHandler(ME, self.base_handlers.cmd_me)
        yield CommandHandler(SET_PASSWORD, self.base_handlers.cmd_set_password)

        yield CommandHandler(MONEY, self.money_transfer_handlers.cmd_money)
        yield CommandHandler(TRANSFER, self.money_transfer_handlers.cmd_transfer_user_money)
        yield CommandHandler(GET_CARD_STATEMENT, self.money_transfer_handlers.cmd_get_card_statement)
        yield CommandHandler(GET_BANK_ACCOUNT_STATEMENT, self.money_transfer_handlers.cmd_get_bank_account_statement)
        yield CommandHandler(GET_STATEMENTS_USERNAMES, self.money_transfer_handlers.cmd_get_statements_usernames)

        yield CommandHandler(ADD_TO_FAVORITES, self.favorite_users_handlers.cmd_add_to_favorites)
        yield CommandHandler(GET_FAVORITES, self.favorite_users_handlers.cmd_get_favorites_list)
        yield CommandHandler(REMOVE_FAVORITE, self.favorite_users_handlers.cmd_delete_favorite)


class Bot:
    def __init__(self, token: str):
        self.token = token
        self.updater = Updater(token=self.token)
        self.dispatcher = self.updater.dispatcher

    def configure(self, commands: List[CommandHandler]):
        for command in commands:
            self.dispatcher.add_handler(command)

    def run(self):
        self.updater.start_webhook(
            listen="0.0.0.0", port=8443, url_path=self.token, key=PRIV_KEY_PATH, cert=CERT_PATH, webhook_url=WEBHOOK_URL
        )
        time.sleep(1)
        self.updater.bot.set_webhook(WEBHOOK_URL)
        self.updater.idle()
